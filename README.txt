--------Welcome to PicOS--------

Author: Joe Roberts
Language: ARM V6 Assembly
Compiler: GNU
Device: Raspberry pi Model B

Raspberry Pi Operating System - picOS -

This project was purely for educational purposes. It follows the baking pi Cambridge tutorial
written by Alex Chadwick http://www.cl.cam.ac.uk/projects/raspberrypi/tutorials/os/.

It is a small terminal based operating system written in the ARMv6 Assembly code, deployed to 
a Raspberry pi micro-computer. It was compiled with GNU and uses a third party USB api to 
connect keyboards and mice. 

Source code can be found here https://bitbucket.org/joeymkr/raspberry-pi-os.

NOTE: This OS has not been updated to be used with Raspberry pi Models A+ & B+. Some elements &
features may not work.
